# Katawa Android Project

### Releases

- ( ) Original
- (x) Lite-version (Assets comprimidos)
- ( ) Lite-no-video (Assets comprimidos e sem vídeos)

### Checklist

- (x) Extração dos scripts
- (x) Extração dos assets
- (x) Compressão PNG
- (x) Compressão JPG
- (x) Implementação dos vídeos
- (x) Correção nos métodos depreciados
- (x) Teste de rota
- (x) Checagem de qualidade
- ( ) Checagem de compatibilidade
- ( ) Releases